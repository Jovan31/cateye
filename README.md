# cateye #
## Tool for measuring CaT indices from integrated spectra ##

This is my own tool for measuring the CaT absorption index from integrated spectra of Local Group globular clusters. 

The main code here is called *cateye.py*. The functions that it is built on are in *cateyeModules.py.*
The algorithm that that *cateye* follows is this:

1. A spectrum (.fits) is read into the code;
2. Using ppxf (http://www-astro.physics.ox.ac.uk/~mxc/software/, Cappellari+04), the input spectrum is fitted by a model. This is very important for spectra that have low signal-to-noise;
3. The model is than Doppler-shifted to be in our rest frame (i.e. to have 0 velocity);
4. The continuum of that model is fit next. By default the code is trying to fit a Chebyshev model of order 8. The regions in which prominent absorption lines are present are ignored during the continuum fitting process;
5. A diagnostic plot is created during the next step (example below);
6. The index is measured next. To do this, each CaT line is fit by a Voigt profile, which is then integrated between the index definition limits. The result from this process is the sum of all 3 CaT lines measurements;
7. The uncertainty of the measurement is determined next. This is done by generating N mock spectra from the input spectrum in a Monte Carlo fashion, and for each of them the CaT index is measured like above. From the resulting distribution the the 16 and 84 percentiles are taken to represent the upper and lower error bars on the central EW index measurement (done on the original spectrum);
8. Finally, the results are saved to file.

The sidekick code *catraw.py*, follow a similar approach as above. The only difference is that step 2 is skipped completely (no spectral fitting is done what so ever), and during step 6, the absorption lines are not fit with a function, but the flux in the pixels between the index definitions are simply summed. Step 7 is also modified to follow the measuring method of step 6.

### Example: a diagnostic plot ###
This is an integrated spectrum of an M31 globular cluster. The black line is the original spectrum, while the thicker red line is the fitted model. The CaT lines are marked with green bands, while some of the more prominent Paschen lines are marked with the grey bands.

![stackH1.jpg](https://bitbucket.org/repo/dB4jMB/images/2216831468-stackH1.jpg)