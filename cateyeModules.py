import sys
import glob
import datetime
import pylab as p
from numpy import *
from time import clock
import progressbar as pb
from scipy import ndimage
import astropy.units as u
from corner import quantile
import astropy.io.fits as pf
import astropy.constants as ac
from scipy.integrate import simps
from readcol import readcol as rcol
import scipy.interpolate as interpolate
from matplotlib.patches import Rectangle
from astropy.modeling import models as astmod
from astropy.modeling import fitting as astfit

# now add the ppxf package
# sys.path.append('/data/users/jovan/PyPacks/ppxf/')
sys.path.append('/Users/Jovan/PyPacks/ppxf/')
import ppxf_util
from ppxf import ppxf

'''
Here I write the functions that are used to create the code that measures the
CaT index on the M31 GC spectral data that I have.
'''

################################################################################
### Setting up the template spectral library
### This instance is set up for the synthetic SSP library of Vazdekis
def setup_spectral_library_vazdekis(velscale, FWHM_targ=3.0):
  '''
  This sets up the template synthetic SSP spectral library from Vazdekis

  arguments:
  velscale  = velocity scale
  FWHM_targ = the full width half maximum of the target spectrum

  returns:
  templates      = the flux of the templates arrange in a regular grid in
                   logAge and metallicity
  waverange_temp = the wavelength range of the templates
  wavearray      = the wavelength array of the templates
  logAge_grid    = the grid storing the logAge
  metal_grid     = the grid storing the metallicity
  '''
  # Set the paths to the templates
  vazdekis = glob.glob('/data/users/jovan/disk1/jv/Spectroscopy/EW/CaT/SSP-Templates-Vazdekis/Cbi*.fits')
  vazdekis.sort()

  # The FWHM of the templates, this is for the Vazdekis library only!
  FWHM_temp = 3.0

  # Extract the wavelength range and logarithmically rebin one template
  # spectrum to the same velocity scale as that of the target spectrum. This
  # will determine the size needed for the array which will contain the template
  # spectra.

  # read the spectrum object
  hdu  = pf.open(vazdekis[0])
  # the data
  spec = hdu[0].data
  # the header
  head = hdu[0].header
  # the range of the wavelength array of the template
  waverange_temp = head['CRVAL1']+array([0.,head['CDELT1']*(head['NAXIS1']-1)])
  # now rebin on a log scale
  specLog, waveLog, velscale = ppxf_util.log_rebin(waverange_temp,
                                                   spec,
                                                   velscale=velscale)

  # Create a 3-D array to store the two dimensional grid
  # of the model (template) spectra
  nAges  = 19
  nMetal = 8
  templates = empty((specLog.size, nAges, nMetal))

  # Convolve the whole Vazdekis library of spectral templates with the
  # quadratic difference between the target and the template spectral
  # resolution. Logarithmically rebin and store each template as a column in
  # the array 'templates'.

  # This assumes that the spectral profiles are well approximated by Gaussians.

  FWHM_diff = (FWHM_targ**2.0 - FWHM_temp**2.0)**0.5
  # the difference in pixels
  pixe_diff = FWHM_diff/2.355/head['CDELT1']

  # These are the arrays that need to be stored, the characteristics of each
  # SSP model template
  logAge_grid = empty((nAges, nMetal))
  metal_grid  = empty((nAges, nMetal))
  # Also keep track of the wavelength dimension of the model. This may be
  wavearray = empty((specLog.size, nAges, nMetal))

  # These are the characteristics of the adopted rectangular grid of SSP models
  logAge = arange(5, 14.5, 0.5)
  metal = array([-2.27, -1.79, -1.49, -1.26, -0.96, -0.66, -0.35, -0.25])

  # Now make sure that the template spectra are sorted in both [M/H] and Age
  # along the two axes of the rectangular grid of templates. A simple
  # alphabetical ordering of Vazdekis's naming convention does not guarantee
  # the files will be sorted by [M/H], so this is done explicitly here.

  metal_name = ['m2.27', 'm1.79', 'm1.49', 'm1.26', 'm0.96', 'm0.66', 'm0.35', 'm0.25']
  for k, m_n in enumerate(metal_name):
    files = [s for s in vazdekis if m_n in s]
    for j, filename in enumerate(files):
      hdu  = pf.open(filename)
      spec = ndimage.gaussian_filter1d(hdu[0].data, pixe_diff)
      specLog, waveLog, velscale = ppxf_util.log_rebin(waverange_temp,
                                                       spec,
                                                       velscale=velscale)
      templates[:,j,k] = specLog # The templates are not normalized here..
      wavearray[:,j,k] = waveLog # The wavelength of the template (log scale)
      logAge_grid[j,k] = logAge[j]
      metal_grid[j,k]  = metal[k]

  # this is the end of this function
  return templates, waverange_temp, wavearray, logAge_grid, metal_grid



################################################################################
### This sets up the spectral library
### This uses the real spectral library of Cenarro+2011
def setup_spectral_library_cenarro(velscale, FWHM_targ=3.0):
  # reading in the table description file
  # table = rcol('/data/users/jovan/disk1/jv/Spectroscopy/EW/CaT/Stellar-Library-Cenarro/Description/myTable',
  table = rcol('/Users/Jovan/Work/Spectroscopy/EW/CaT/Stellar-Library-Cenarro/Description/myTable',
               twod=False,
               verbose=False)
  # The properties of the stars in the library
  specID  = table[0]
  specFeH = table[3]
  specCaT = table[4]

  # if a star has a Fe/H larger than -0.2 and CaT larger than 15, exlude it
  ind = (specFeH<=-0.2) & (specCaT<15.)
  specID  = specID[ind]
  specFeH = specFeH[ind]
  specCaT = specCaT[ind]

  # FWHM of the templates
  FWHM_temp = 1.5

  # the location of the actual fits files
  # database = '/data/users/jovan/disk1/jv/Spectroscopy/EW/CaT/Stellar-Library-Cenarro/scan'
  database = '/Users/Jovan/Work/Spectroscopy/EW/CaT/Stellar-Library-Cenarro/scan'

  # Extract the wavelength range and logarithmically re-bin one spectrum to the
  # to the same velocity scale as the target spectrum, to determine the size
  # needed for the array which will contain the flux of the template spectra

  hdu  = pf.open(database+str('%03i' % specID[0])+'.fits')
  flux = hdu[0].data[0]
  head = hdu[0].header
  waveRange_temp = head['CRVAL1']+array([0.,head['CDELT1']*(head['NAXIS1']-1)])
  fluxLog, waveLog, velscale = ppxf_util.log_rebin(waveRange_temp,
                                                   flux,
                                                   velscale=velscale)

  # Now convolve the Cenarro stellar library with with the quadratic difference
  # between the target and the template instrumental resolution.
  # Logarithmically re-bin and store each template as a column in the array
  # 'templates'

  # Quadratic sigma difference in pixels.
  # The formula below is valid if the shapes of the instrumental spectral
  #profiles are well approximated by Gaussians.

  FWHM_dif = (FWHM_targ**2. - FWHM_temp**2.)**0.5
  sigma    = FWHM_dif/2.355/head['CDELT1'] # Sigma difference in pixels

  # The array in which the final flux arrays of each templates are stored
  templates = empty((fluxLog.size, len(specID)))
  # Same but for the wavelength
  wavearray = empty((fluxLog.size, len(specID)))

  # Storing the templates
  for i,v in enumerate(specID):
          hdu  = pf.open(database+str('%03i' % v)+'.fits')
          flux = ndimage.gaussian_filter1d(hdu[0].data[0], sigma)
          specLog, waveLog, velscale = ppxf_util.log_rebin(waveRange_temp,
                                                           flux,
                                                           velscale=velscale)
          templates[:,i] = specLog # The templates are not normalized here..
          wavearray[:,i] = waveLog # The wavelength of the template (log scale)
  # this is the end of this function
  return templates, waveRange_temp, wavearray

################################################################################
### This sets up the target
def setup_spectral_target(wave, flux, nois, FWHM_targ=3.0, native_noise=False):
  '''
  This sets up the target for the upcoming procedure.
  This is also necessary to fully set up the spectral template library.

  arguments:
  wave         = the wavelength array of the target
  flux         = the flux array of the target
  nois         = the noise/error array of the target
  WFHM_targ    = the full width half maximum of the target spectrum;
  noise_native = If True, the Poisson noise from the optimal extraction of the
                 IRAF spectra  will be used as errors in the spectral fitting.
                 If False, constant noise will be used, adopted form the pPXF
                 example.

  returns:
  nflux        = the prepared flux of the target, normalized by the constant
                 value (the median of the whole spectrum);
  wave         = the wavelength array of the target spectrum;
  velscale     = the velocity scale of the target;
  noise        = the flux noise array;
  waveLog      = the wavelength array that was logarithmically re-binned.
  '''

  # the speed of light
  c = ac.c.to(u.km/u.s).value

  # calculate the velocity scale
  velscale = log(wave[1]/wave[0])*c

  # Finding the common wavelength range between the templates and the target
  ind = (wave > 8400.) & (wave < 8840.)
  wave = wave[ind]
  flux = flux[ind]
  nois = nois[ind]

  # Rebin the flux and wavelength into the log scale needed
  waverange_targ = [wave[0], wave[-1]]
  fluxLog, waveLog, velscale = ppxf_util.log_rebin(waverange_targ,
                                                   flux,
                                                   velscale=velscale)
  # rebin the noise flux into the log  scale needed
  noisLog, temp1, temp2      = ppxf_util.log_rebin(waverange_targ,
                                                   nois,
                                                   velscale=velscale)

  # Normalize the target spectrum by a constant (the median of the full spec)
  # This is done to avoid numerical issues
  nflux = fluxLog/median(fluxLog)

  # Setting up the noise
  if native_noise:
    noise = noisLog
  else:
    noise = nflux*0 + 0.01528

  return nflux, wave, velscale, noise, waveLog

################################################################################
### Here the pPXF is run, and all the set up is done for it
def run_ppxf(wave, flux, nois, vel,
             FWHM_targ=3.0, native_noise=False, verbose=False,
             spec_lib='Cenarro01'):
  '''
  This runs the spectral fitting with the pPXF code of Capellari+2004.

  arguments:
  wave      = the wavelength array of the target;
  flux      = the flux array of the target;
  nois      = the noise/error array of the target;
  vel       = the guess velocity of the target, in km/s;
  WFHM_targ = the full width half maximum of the target spectrum;
  verbose   = bool, if true, the best fit values will be displayed on screen;
  spec_lib  = the spectral template library. Currently, only the stellar
              library of Cenarro+01, and the synthetic SSP models of Vazdekis
              are supported.

  returns:
  pp        = the fitted model, the output of the pPXF routine.
  waveLog   = the target wavelength array that was logarithmically re-binned.
  '''

  # The speed of light in km/s
  c = ac.c.to(u.km/u.s).value

  # set the target spectrum up
  temp = setup_spectral_target(wave, flux, nois, FWHM_targ,
                               native_noise=native_noise)
  flux, wave, velscale, noise, waveLog = temp
  # clean up
  del temp

  # set the spectral library up
  if spec_lib=='Cenarro01':
    temp = setup_spectral_library_cenarro(velscale, FWHM_targ)
  elif spec_lib=='ArmandoffZinn88':
    temp = setup_spectral_library_vazdekis(velscale, FWHM_targ)
  else:
    print('ERROR: please choose a supported stellar library.')
    sys.exit()
  templates, waverange_temp, wavearray = temp
  # clean up
  del temp

  # Correct the offset between the target and the template spectra for them not
  # having the identical wavelength solution or starting wavelength
  dv = c * log(waverange_temp[0]/wave[0])

  # Determine the pixels that are good enough for the spectral fitting.
  # For example regions that are problematic (skylines etc..) should not
  # excluded from the 'goodpixels' array.
  goodpixels = arange(len(flux))

  # Normalize the templates, same as for the target: by their median value
  templates = templates/median(templates)

  # The regularization error (see pPXF Docs for details)
  inv_regul_err = 0

  # The initial guesses for the radial velocity and velocity dispersion of the
  # target. For the velocity dispersion, a constant guess of 5 km/s is
  # hard-coded.
  start = [vel, 5.]

  # The magic happens here
  pp = ppxf(templates,
            flux,
            noise,
            velscale,
            start,
            bias=None,
            goodpixels=goodpixels,
            plot=False,
            moments=6,
            degree=-1,
            vsyst=dv,
            clean=False,
            mdegree=10,
            regul=inv_regul_err,
            quiet=not(verbose))


  if verbose:
    print('Desired Delta Chi^2: %.4g' % sqrt(2*goodpixels.size))
    print('Current Delta Chi^2: %.4g' % ((pp.chi2 - 1)*goodpixels.size))

    # print('Mass-weighted <logAge> [Gyr]: %.3g' %
    #       (sum(pp.weights*logAge_grid.ravel())/sum(pp.weights)))
    # print('Mass-weighted <[M/H]>: %.3g' %
    #       (sum(pp.weights*metal_grid.ravel())/sum(pp.weights)))

  return pp, waveLog




################################################################################
### This is a simple Doppler shifting interpolation function..
def dopplerShift(wave, flux, vel):
  '''
  This is a task for Doppler shifting a spectrum by some velocity [km/s]
  arguments:
  wave = the wavelength array of the spectrum
  flux = the flux array of the spectrum
  vel  = the velocity by which the spectrum is to be shifted. vel is assumed to be in km/s.

  returns:
  fluxShift = the Doppler shifted flux
  waveShift = the Doppler shifted wavelength
  '''

  # The speed of light in km/s
  c = ac.c.to(u.km/u.s).value

  # shifting the wavelength grid
  waveShift = wave * sqrt(1. - vel/c)/sqrt(1. + vel/c)

  # interpolating (spline3 interpolation)so that the flux is aligned with the new wavelength array
  fluxShift = interpolate.splev(waveShift, interpolate.splrep(waveShift, flux, s=0), der=0)
  # dealing with the edges..
  # for now the edges are just removed.. it may or may not cause issues in the future..
  fluxShift = fluxShift[30:-30]
  waveShift = waveShift[30:-30]

  #all done
  return fluxShift, waveShift



################################################################################
### This is a function that fits a continuum to the CaT portion of the spectrum
def contFit(wave, flux, clip_up, clip_low):
  '''
  This function iteratively fits the continuum of a spectrum. After each fit
  the points (pixels) deviating more than clip_up or clip_down percentage are
  rejected and the fit is done again. This is repeated until no more points are
  rejected.

  Prior to any fitting, known strong absorption lines are masked (CaT, known iron and Paschen lines).
  This is only for the region between 8440 - 8840 A.

  arguments:
  wave      = array_like, the wavelength dimension of the spectrum
  flux      = array_like, the flux of the spectrum
  clip_up   = float, pixels with this many percent above the model are rejected
  clip_low  = float, pixels with this many percent below the model are rejected
             (note that this needs to be a negative value)

  returns:
  contNorm  = the continuum normalized flux
  '''

  # Save the original arrays for later use
  oflux = flux
  owave = wave

  # This is the mask which masks the regions of strong absorption lines
  # (the definitions of the lines may be changed in the future)
  mask = (((8490<wave) & (wave<8506)) |
          ((8532<wave) & (wave<8552)) |
          ((8653<wave) & (wave<8670)) |
          ((8432<wave) & (wave<8437)) |
          ((8464<wave) & (wave<8472)) |
          ((8511<wave) & (wave<8519)) |
          ((8672<wave) & (wave<8677)) |
          ((8686<wave) & (wave<8691)) |
          ((8802<wave) & (wave<8810)) |
          ((8821<wave) & (wave<8827)))

  # This mas is supposed to exclude regions, so make it negative
  mask = invert(mask)

  # The wavelength and flux after masking
  flux = flux[mask]
  wave = wave[mask]

  # Here starts the iterative continuum fitting..

  # while this flip is off, the itterating fitting/rejection loop is active;
  # once the flip is turned on, the algorithm continues beyond the loop
  flip = False

  while flip==False:
    # Define the model
    mod = astmod.Chebyshev1D(degree=8)
    # Choose a suitable fitter
    fit = astfit.LevMarLSQFitter()
    # Fit the model with the data
    modfit = fit(mod, wave, flux)
    # This is the continuum so far
    cont = modfit(wave)
    # the residuals
    residuals = (flux-cont)/cont * 100.
    # finding the points the be rejected
    ind = (residuals<clip_up) & (residuals>clip_low)
    # applying the mask to the flux and wave for the next iteration if needed
    wave = wave[ind]
    flux = flux[ind]
    # checking the flip
    flip = ind.all()

  # return continuum of the final iteration, full wavelength range
  return modfit(owave)



################################################################################
### This is a function that makes the nice pretty diagnostic plot
def specPlot(wave, fdat, ffit, oname):
  '''
  This function makes a nice diagnostic plot for the continuum normalized
  fitted and unfitted spectrum. The strongest features, such as the CaT and
  other lines are marked.

  arguments:
  wave  = the wavelength scale
  fdat  = the flux of the unfitted spectrum
  ffit  = the flux of the fitted spectrum
  oname = the full path and name of the plot to be saved to disk

  return:
  '''

  # setting up the figure
  figga = p.figure(figsize=(12,9))

  # Axes labels
  p.xlabel(r'Wavelength [$\AA$]', fontsize=18)
  p.ylabel('Normalized counts', fontsize=18)

  # Formating the axes
  p.gca().tick_params(axis='both', which='major', labelsize=14)

  # Plotting the fitted and the 'raw' spectra
  p.plot(wave, ffit, lw=2,   c='red',   label='fitted model')
  p.plot(wave, fdat, lw=0.5, c='black', label='unfitted spectrum')

  # Deciding the on the axes ranges
  ymin = min(min(ffit), min(fdat))-0.05
  ymax = max(fdat+0.01)
  ydif = ymax - ymin
  p.ylim(ymin, ymax)
  p.xlim(min(wave), max(wave))

  # Defining the masked regions for the purpose of the plot
  # The CaT lines
  CaT1 = Rectangle((8490,ymin), width=16, height=ydif, color='lightblue', ec='None', alpha=0.7)
  CaT2 = Rectangle((8532,ymin), width=20, height=ydif, color='lightblue', ec='None', alpha=0.7)
  CaT3 = Rectangle((8653,ymin), width=17, height=ydif, color='lightblue', ec='None', alpha=0.7)
  # The other lines
  oth1 = Rectangle((8432,ymin), width=5, height=ydif, color='lightgray', ec='None', alpha=0.7)
  oth2 = Rectangle((8464,ymin), width=8, height=ydif, color='lightgray', ec='None', alpha=0.7)
  oth3 = Rectangle((8511,ymin), width=8, height=ydif, color='lightgray', ec='None', alpha=0.7)
  oth4 = Rectangle((8672,ymin), width=5, height=ydif, color='lightgray', ec='None', alpha=0.7)
  oth5 = Rectangle((8686,ymin), width=5, height=ydif, color='lightgray', ec='None', alpha=0.7)
  oth6 = Rectangle((8802,ymin), width=8, height=ydif, color='lightgray', ec='None', alpha=0.7)
  oth7 = Rectangle((8821,ymin), width=8, height=ydif, color='lightgray', ec='None', alpha=0.7)
  # adding all the patches to the plot
  p.gca().add_patch(CaT1)
  p.gca().add_patch(CaT2)
  p.gca().add_patch(CaT3)
  p.gca().add_patch(oth1)
  p.gca().add_patch(oth2)
  p.gca().add_patch(oth3)
  p.gca().add_patch(oth4)
  p.gca().add_patch(oth5)
  p.gca().add_patch(oth6)
  p.gca().add_patch(oth7)

  # the legend
  p.legend(loc='lower right', fontsize=16)
  p.savefig(oname, dpi=120, bbox_inches='tight')
  p.clf()



################################################################################
### This is the function that measures the CaT index
def catEW(wave, flux, line_def='ArmandoffZinn88'):
  '''
  This functions measures the CaT equivalent width. To each CaT line,
  a Voigt profile is being fit, and than the stuff is integrated between
  the line definitions of each line.

  This function currently uses the Cenarro+01 line definitions.

  The Armandoff & Zinn 1988 line definitions:
  ind = (wave >= 8490) & (wave <= 8506)
  ind = (wave >= 8532) & (wave <= 8552)
  ind = (wave >= 8653) & (wave <= 8671)

  The Cenarro+01 line definitions:
  ind = (wave >= 8484.) & (wave <= 8513.)
  ind = (wave >= 8522.) & (wave <= 8562.)
  ind = (wave >= 8642.) & (wave <= 8682.)

  arguments:
  wave     = the wavelength array
  flux     = the continuum normalized flux array
  line_def = the line definitions. Currently, only the definitions from
             Armandoff & Zinn 1988 and Cenarro+01 are supported.

  returns:
  res      = the CaT equivalent width, sum of all 3 CaT lines.
  '''

  # Choosing the line definitions
  if line_def=='ArmandoffZinn88':
    ind1 = (wave >= 8490.) & (wave <= 8506.)
    ind2 = (wave >= 8522.) & (wave <= 8562.)
    ind3 = (wave >= 8653.) & (wave <= 8671.)
  elif line_def=='Cenarro01':
    ind1 = (wave >= 8484.) & (wave <= 8513.)
    ind2 = (wave >= 8522.) & (wave <= 8562.)
    ind3 = (wave >= 8642.) & (wave <= 8682.)
  else:
    print('ERROR: Please choose a supported line definition system.')
    sys.exit()

  # For the fitting to work, the continuum must be at a 0 level.
  # So this just shift the continuum level to 0
  flux = flux-1.

  # Define the fitter what will be used for all 3 CaT lines
  vf   = astfit.LevMarLSQFitter()

  # The CaT1:
  w1   = wave[ind1]
  f1   = flux[ind1]
  # Define the Voigt profile for the 1st CaT line
  vm1  = astmod.Voigt1D(x_0=8498., amplitude_L=-0.5)
  # Fitting the model to the data
  vm1  = vf(vm1, w1, f1)
  # Integrating along the model to find the EW of CaT1
  cat1 = simps(y=vm1(w1), x=w1)

  # The CaT2:
  w2   = wave[ind2]
  f2   = flux[ind2]
  # Define the Voigt profile for the 1st CaT line
  vm2  = astmod.Voigt1D(x_0=8542., amplitude_L=-0.5)
  # Fitting the model to the data
  vm2  = vf(vm2, w2, f2)
  # Integrating along the model to find the EW of CaT2
  cat2 = simps(y=vm2(w2), x=w2)

  # The CaT3:
  w3   = wave[ind3]
  f3   = flux[ind3]
  # Define the Voigt profile for the 1st CaT line
  vm3  = astmod.Voigt1D(x_0=8662., amplitude_L=-0.5)
  # Fitting the model to the data
  vm3  = vf(vm3, w3, f3)
  # Integrating along the model to find the EW of CaT3
  cat3 = simps(y=vm3(w3), x=w3)

  # Summing up the contributions from each CaT line
  # The absolute value is to correct for the continuum
  # being at 0 rathar than 1. Oh I hope this is right..
  res   = abs(cat1 + cat2 + cat3)

  # return the result
  return res




################################################################################
# Push button receive bacon.
def run_cat(target, datadir, vel, FWHM_targ,
            native_noise=True, verbose=False, logfile=False,
            spec_lib='Cenarro01', line_def='ArmandoffZinn88'):
  '''
  This is a wrapper, it receives a target spectrum in a IRAF multispec format
  (4 dimensions assumed). It goes through the whole pipeline and it returns the
  CaT index and the associated uncertainty.

  arguments:
  target    = the target fits file. Iraf multispec format is assumed;
  datadir   = the location of the fits files;
  vel       = the guess velocity of the target, in km/s;
  WFHM_targ = the full width half maximum of the target spectrum;
  verbose   = bool, if True, the best fit values will be displayed on screen;
  logfile   = bool, if True, the output of the pPXF is saved to a log file;
  spec_lib  = the spectral library to be used. Currently, only the stellar
              library from Cenarro+01 and the synthetic SSP templates from
              Vazdekis are supported;
  line_def  = the line definitions. Currently, only the Armandoff & Zinn 1988
              and Cenarro+01 are supported.


  returns:
  EWfit     = the CaT index measured on the fitted model;
  EWdat     = the CaT index measured on the unfitted spectrum;
  EWfit_err = the lower and upper limits on the fitted CaT measurements;
  EWdat_err = the lower and upper limits on the unfitted CaT measurements.
  '''

  # reading in the target
  hdu  = pf.open(datadir+target)
  flux = hdu[0].data[0][0]
  nois = hdu[0].data[3][0]
  head = hdu[0].header
  wave = head['CRVAL1'] + head['CD1_1']*arange(head['NAXIS1'])

  # Do the full spectral fitting
  pp, waveLog = run_ppxf(wave,
                         flux,
                         nois,
                         vel,
                         FWHM_targ=FWHM_targ,
                         native_noise=native_noise,
                         verbose=verbose,
                         spec_lib=spec_lib)

  # If needed, save the results of pPXF to a log file
  if logfile:
    logger(target, pp, datadir)

  # Shift the spectrum and fitted model to 0 velocity
  zvfit, zvwave = dopplerShift(exp(waveLog), pp.bestfit,  pp.sol[0])
  zvdat, zvwave = dopplerShift(exp(waveLog), pp.galaxy,   pp.sol[0])

  # Fit the continuum to the target spectrum and fitted model
  cfit = contFit(zvwave, zvfit, clip_up=1., clip_low=-1.)
  cdat = contFit(zvwave, zvdat, clip_up=1., clip_low=-1.)

  # Diagnostic plot
  oname = datadir+target[:-5]+'.pdf'
  specPlot(zvwave, zvdat/cdat, zvfit/cfit, oname=oname)

  # Calculate the CaT index, both on the 'raw' spectrum and fitted model
  CaTfit = catEW(zvwave, zvfit/cfit, line_def=line_def)
  CaTdat = catEW(zvwave, zvdat/cdat, line_def=line_def)

  # Calculate the uncertainties
  CaTfit_err, CaTdat_err = ewMCuncertainty(wave, flux, nois, vel,
                                           FWHM_targ=FWHM_targ,
                                           native_noise=native_noise,
                                           iters=100,
                                           spec_lib=spec_lib,
                                           line_def=line_def)

  # Return the results.
  # The errors are relative to the central measurement [low, high]
  return CaTfit, CaTdat, CaTfit_err, CaTdat_err







################################################################################
### This is the logger function that saves the results of the pPXF code to a
### file. The output is the same as if the 'verbose' keyword is True.
def logger(target, pp, datadir):
  '''
  This is a logger function that saves the results of the pPXF code to a file.
  The output is the same as if the 'verbose' keyword was set to True in the
  run_cat function.

  parameters:
  target  = the ID/name/label of the object that is logged;
  pp      = this is the associated object returned from the ppxf routine;
  datadir = the location where the log will be saved on the disk.
  '''

  fout = open(datadir+'cateyeLog', 'a')
  print(target, file=fout)
  print('%10s %10s %10s %10s %10s %10s %10s' % ('Best Fit:',
                                                'V',
                                                'sigma',
                                                'h3',
                                                'h4',
                                                'h5',
                                                'h6'),
                                                file=fout)
  print('%10s %10.3g %10.3g %10.3g %10.3g %10.3g %10.3g' % ('  ',
                                                            pp.sol[0],
                                                            pp.sol[1],
                                                            pp.sol[2],
                                                            pp.sol[3],
                                                            pp.sol[4],
                                                            pp.sol[5]),
                                                            file=fout)
  print('Desired Delta Chi^2: %.4g' % sqrt(2*pp.goodpixels.size), file=fout)
  print('Current Delta Chi^2: %.4g' % ((pp.chi2 - 1)*pp.goodpixels.size),
        file=fout)
  print('Fit completed on', datetime.datetime.now(), file=fout)
  print('', file=fout)
  fout.close()



################################################################################
### This function generates a spectrum in a MC way from the input spectrum and
### the accompanying noise spectrum
def specGen(wave, flux, nois):
  '''
  Generates a spectrum in a MC fashion from the provided flux and noise arrays.
  The flux and nois arrays should be calibrated on the same wavelength scale.
  For each wavelenth element, the flux (counts) values is randomly (uniformly)
  changed within the interval of the noise (+/- of the noise value at that pixel.

  The wave, flux and nois should have the same dimension.
  The new, generated flux is returned. It is not normalized in any fashion.s

  arguments:
  wave = the wavelength array
  flux = the flux array of the spectrum
  nois = the noise, error array of the spectrum

  returns:
  newFlux = the newly generated flux.
  '''

  # The number of pixel elements in the spectrum
  l = len(wave)

  # Creating the resulting array
  newFlux = zeros(l)

  # The master loop. This is where the magic happens
  for i in range(l):
    # choose a random value to be added to the flux
    df = random.uniform(low=-nois[i], high=nois[i])
    # add the extra noise to the original flux
    newFlux[i] = flux[i] + df

  # return the baby
  return newFlux



################################################################################
### This function is created to estimate the uncertainty of the EW measurements
def ewMCuncertainty(wave, flux, nois, vel, FWHM_targ=3.,
                    native_noise=True, iters=100,
                    spec_lib='Cenarro01',
                    line_def='ArmandoffZinn88'):
  '''
  This function estimates the uncertainty in the EW measurements by a MC
  process. For each input spectrum, 'itter' spectra are generated in a MC way
  from the noise. For each of them the EW index is calculated. For the resulting
  distribution, the 16 and 84 percentiles are taken to represent the upper and
  lower error bars on the central EW index measurement (done on the original
  spectrum)

  arguments:
  wave         = the wavelength array
  flux         = the flux array of the spectrum
  nois         = the noise, error array of the spectrum
  vel          = the radial velocity (or guess) of the target object;
  FWHM_targ    = the full width half maximum of the target spectrum;
  noise_native = If True, the Poisson noise from the optimal extraction of the
                 IRAF spectra  will be used as errors in the spectral fitting.
                 If False, constant noise will be used, adopted form the pPXF
                 example;
  iters        = the number of MC generated spectra;
  spec_lib     = the spectral library to be used. Currently, only the stellar
                 library from Cenarro+01 and the synthetic SSP templates from
                 Vazdekis are supported;
  line_def     = the line definitions. Currently, only the Armandoff & Zinn 1988
                 and Cenarro+01 are supported.

  returns:
  a tuple contain
  '''

  # This stores the EW measurements of each iteration
  EWs_fit = zeros(iters)
  EWs_dat = zeros(iters)

  # The master loop.. this generates the MC spectra and calculates the CaT index
  for i in range(iters):
    # generate a MC spectrum
    genFlux = specGen(wave, flux, nois)
    # do the spectral fitting
    pp, waveLog = run_ppxf(wave,
                           genFlux,
                           nois,
                           vel,
                           FWHM_targ=FWHM_targ,
                           native_noise=native_noise,
                           verbose=False,
                           spec_lib=spec_lib)
    zvfit, zvwave = dopplerShift(exp(waveLog), pp.bestfit,  pp.sol[0])
    zvdat, zvwave = dopplerShift(exp(waveLog), pp.galaxy,   pp.sol[0])

    # Fit the continuum to the target spectrum and fitted model
    cfit = contFit(zvwave, zvfit, clip_up=1., clip_low=-1.)
    cdat = contFit(zvwave, zvdat, clip_up=1., clip_low=-1.)

    # Measure the EWs
    EWs_fit[i] = catEW(zvwave, zvfit/cfit, line_def=line_def)
    EWs_dat[i] = catEW(zvwave, zvdat/cdat, line_def=line_def)


  # Now find the .16th and .84th quantile of the distribution, and those are
  # the errors..
  return quantile(EWs_fit, [.16, .84]), quantile(EWs_dat, [.16, .84])



################################################################################
### This is the function that measures the CaT index, but without fitting any
### spectral shapes to the lines. The flux is simply summed up.
def catEWraw(wave, flux, line_def='ArmandoffZinn88'):
  '''
  This functions measures the CaT equivalent width. The shape/profile of the
  lines are not fit, but the flux is simply summed within the index
  definitions.

  This function currently uses the Cenarro+01 line definitions.

  The Armandoff & Zinn 1988 line definitions:
  ind = (wave >= 8490) & (wave <= 8506)
  ind = (wave >= 8532) & (wave <= 8552)
  ind = (wave >= 8653) & (wave <= 8671)

  The Cenarro+01 line definitions:
  ind = (wave >= 8484.) & (wave <= 8513.)
  ind = (wave >= 8522.) & (wave <= 8562.)
  ind = (wave >= 8642.) & (wave <= 8682.)

  arguments:
  wave     = the wavelength array
  flux     = the continuum normalized flux array
  line_def = the line definitions. Currently, only the definitions from
             Armandoff & Zinn 1988 and Cenarro+01 are supported.

  returns:
  res      = the CaT equivalent width, sum of all 3 CaT lines.
  '''

  # Choosing the line definitions
  if line_def=='ArmandoffZinn88':
    ind1 = (wave >= 8490.) & (wave <= 8506.)
    ind2 = (wave >= 8522.) & (wave <= 8562.)
    ind3 = (wave >= 8653.) & (wave <= 8671.)
  elif line_def=='Cenarro01':
    ind1 = (wave >= 8484.) & (wave <= 8513.)
    ind2 = (wave >= 8522.) & (wave <= 8562.)
    ind3 = (wave >= 8642.) & (wave <= 8682.)
  else:
    print('ERROR: Please choose a supported line definition system.')
    sys.exit()

  # For the fitting to work, the continuum must be at a 0 level.
  # So this just shift the continuum level to 0
  flux = flux-1.

  # The CaT1:
  w1   = wave[ind1]
  f1   = flux[ind1]
  # Integrating all the flux within the line index
  cat1 = simps(y=f1, x=w1)

  # The CaT2:
  w2   = wave[ind2]
  f2   = flux[ind2]
  # Integrating all the flux within the line index
  cat2 = simps(y=f2, x=w2)

  # The CaT3:
  w3   = wave[ind3]
  f3   = flux[ind3]
  # Integrating all the flux within the line index
  cat3 = simps(y=f3, x=w3)

  # Summing up the contributions from each CaT line
  # The absolute value is to correct for the continuum
  # being at 0 rathar than 1. Oh I hope this is right..
  res   = abs(cat1 + cat2 + cat3)

  # return the result
  return res


################################################################################
def ewMCuncertainty_raw(wave, flux, nois, vel,
                        iters=100, line_def='ArmandoffZinn88'):
  '''
  This function estimates the uncertainty in the EW measurements by a MC
  process. For each input spectrum, 'itter' spectra are generated in a MC way
  from the noise. For each of them the EW index is calculated. For the resulting
  distribution, the 16 and 84 percentiles are taken to represent the upper and
  lower error bars on the central EW index measurement (done on the original
  spectrum)

  arguments:
  wave         = the wavelength array
  flux         = the flux array of the spectrum
  nois         = the noise, error array of the spectrum
  vel          = the radial velocity (or guess) of the target object;
  iters        = the number of MC generated spectra;
  line_def     = the line definitions. Currently, only the Armandoff & Zinn 1988
                 and Cenarro+01 are supported.
  returns:
  a tuple containing the 16th and 84h percentiles of the MC distribution.
  '''

  # This stores the EW measurements of each iteration
  EWs = zeros(iters)

  # The master loop.. this generates the MC spectra and calculates the CaT index
  for i in range(iters):
    # generate a MC spectrum
    genFlux = specGen(wave, flux, nois)
    zvflux, zvwave = dopplerShift(wave, genFlux, vel)

    # Fit the continuum to the target spectrum and fitted model
    cflux = contFit(zvwave, zvflux, clip_up=1., clip_low=-1.)

    # The continuum normalized, zero velocity flux
    cnflux = zvflux/cflux

    # Measure the EWs
    EWs[i] = catEWraw(zvwave, cnflux, line_def=line_def)

  # Now find the .16th and .84th quantile of the distribution, and those are
  # the errors..
  return quantile(EWs, [.16, .84])



################################################################################
### Push button receive raw bacon
def run_raw(target, datadir, vel, FWHM_targ,
            native_noise=True, verbose=False, logfile=False,
            spec_lib='Cenarro01', line_def='ArmandoffZinn88'):
  '''
  Doc string to be created soon. Maybe when I have an idea of what is happening.
  '''

  # reading in the target
  hdu  = pf.open(datadir+target)
  flux = hdu[0].data[0][0]
  nois = hdu[0].data[3][0]
  head = hdu[0].header
  wave = head['CRVAL1'] + head['CD1_1']*arange(head['NAXIS1'])

  # Do the full spectral fitting
  pp, waveLog = run_ppxf(wave,
                         flux,
                         nois,
                         vel,
                         FWHM_targ=FWHM_targ,
                         native_noise=native_noise,
                         verbose=verbose,
                         spec_lib=spec_lib)

  # Now that the fitting is done, butcher the spectrum in the same way as during
  # the fitting..
  ind = (wave > 8400.) & (wave < 8840.)
  wave = wave[ind]
  flux = flux[ind]
  nois = nois[ind]

  # Shift the spectrum and fitted model to 0 velocity
  # The ppxf velocity is used here, for consistency with cateye
  zvflux, zvwave = dopplerShift(wave, flux, pp.sol[0])

  # Fit the continuum to the target spectrum and fitted model
  cflux = contFit(zvwave, zvflux, clip_up=1., clip_low=-1.)

  # The continuum normalized, zero velocity flux
  cnflux = zvflux/cflux

  # Measure the CaT index
  # This is where catraw deviates from cateye. In the former, I simply sum up
  # the flux within the index definitions. No line fitting, nada.
  CaT = catEWraw(zvwave, cnflux, line_def=line_def)

  # Calculate the uncertainties
  CaT_err = ewMCuncertainty_raw(wave, flux, nois, pp.sol[0], iters=1000)

  # The end of the taks
  return CaT, CaT_err
