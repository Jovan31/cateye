from cateyeModules import *

'''
This is the code that measures the CaT index in the M31 GCs spectra that I have.
'''

# Preparing the list of targets
database    = '/data/users/jovan/disk1/jv/Spectroscopy/EW/CaT/Gemini/'
targetList  = 'targetList_red'
target, vel = rcol(database+targetList, twod=False)
# Setting up the run
spec_lib    = 'Cenarro01'
line_def    = 'Cenarro01'

# Text file where the final results will be stored in
ff = open(database+'cateyeMeasurements','a')
# Set up the Info header
print('# Target list: ', database+targetList, file=ff)
print('# Spectra template library: ', spec_lib, file=ff)
print('# CaT line definition: ', line_def, file=ff)
ff.flush()
# Set up the columns header
print ('%15s %15s %15s %15s %15s %15s %15s ' % ('# SpectrumID', 
                                                'CaT-fit', 
                                                'CaT-dat', 
                                                'eCaT-fit_low', 
                                                'eCaT-fit_high',
                                                'eCaT-dat_low',
                                                'eCaT-dat_high'), 
                                                file=ff)

# Progress bar: because I want my code to look fancy
pbar = pb.ProgressBar(widgets=[' [', pb.Timer(), '] ', 
                      pb.Bar(),' (', pb.ETA(), ') ',], 
                      max_value = len(target))


# This is the master loop, this runs the code for every input target
for i,v in enumerate(target):
  # The heart of the code
  EWfit, EWdat, EWfit_err, EWdat_err = run_cat(v,
                                               database, 
                                               vel[i], 
                                               FWHM_targ=2.3, 
                                               native_noise=True, 
                                               logfile=True,
                                               verbose=False,
                                               spec_lib=spec_lib,
                                               line_def=line_def)
  # The output results
  print ('%15s %15.5f %15.5f %15.5f %15.5f %15.5f %15.5f' % (v[:-5], 
                                                             EWfit, 
                                                             EWdat, 
                                                             EWfit_err[0],
                                                             EWfit_err[1],
                                                             EWdat_err[0],
                                                             EWdat_err[1]),
                                                            file=ff)
  # Flush the print buffer to the file
  ff.flush()
  # The progress bar :)
  pbar.update(i)

# Close the progress bar once the master loop is through
pbar.finish()

# Close the text file
print('### Measurements completed on:', datetime.datetime.now(), file=ff)
print('### E-o-F', file=ff)
print('', file=ff)
ff.close()

print('')
print('The rent is too DAMN high!')
print ('')
