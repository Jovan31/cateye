from cateyeModules import *

'''
This is the code that measures the CaT index in the M31 GCs spectra that I have.
Unlike the cateye code, this does not do any spectral fitting, nor fits a
fits a function to the CaT lines. It just sums up the flux between the indices,
sort of like what the indexf code is supposedly doing.
'''

# Preparing the list of targets
database    = '/Users/Jovan/Work/Spectroscopy/EW/CaT/WHT09/'
targetList  = 'targetList'
target, vel = rcol(database+targetList, twod=False)
# Setting up the run
spec_lib    = 'Cenarro01'
line_def    = 'Cenarro01'

# Text file where the final results will be stored in
ff = open(database+'catrawMeasurements','a')
# Set up the Info header
print('# Target list: ', database+targetList, file=ff)
print('# Spectra template library: ', spec_lib, file=ff)
print('# CaT line definition: ', line_def, file=ff)
ff.flush()

# Set up the columns header
print ('%15s %15s %15s %15s' % ('# SpectrumID',
                                'CaT-raw',
                                'eCaT-raw_low',
                                'eCaT-raw_high'),
                                file=ff)

# Progress bar: because I want my code to look fancy
pbar = pb.ProgressBar(widgets=[' [', pb.Timer(), '] ',
                      pb.Bar(),' (', pb.ETA(), ') ',],
                      max_value = len(target))

# This is the master loop, this runs the code for every input target
for i,v in enumerate(target):
  # The heart of the code
  EWraw, EWraw_err = run_raw(v,
                             database,
                             vel[i],
                             FWHM_targ=2.3,
                             native_noise=True,
                             logfile=False,
                             verbose=False)

  # The output results
  print ('%15s %15.5f %15.5f %15.5f ' % (v[:-5],
                                         EWraw,
                                         EWraw_err[0],
                                         EWraw_err[1]),
                                        file=ff)
  # Flush the print buffer to the file
  ff.flush()
  # The progress bar :)
  pbar.update(i)

# Close the progress bar once the master loop is through
pbar.finish()

# Close the text file
print('### Measurements completed on:', datetime.datetime.now(), file=ff)
print('### E-o-F', file=ff)
print('', file=ff)
ff.close()

print()
print('The rent is too DAMN high!')
print()